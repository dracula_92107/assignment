# Stock prices services

## Run
- Open command line
- Run: java -jar stockPriceService.jar

# APIs:
* Get close price:
	http://localhost:8080/api/v2/GE/closePrice?startDate=2019-01-28&endDate=2019-01-29
	- GE: ticket symbol of stock
	- startDate: Query from this date
	- endDate: Query to this date


* Get 200 date moving average:
	http://localhost:8080/api/v2/MSFT/200dma?startDate=2018-07-18
	- MSFT: ticket symbol of stock
	- startDate: Query from this date


* Get 200 date moving average of multiple tickets
	http://localhost:8080/api/v2/GE,HD,DIS,MMM,JNJ,CAT,MRK,MSFT/multi200dma?startDate=2019-01-29
	- GE,HD,DIS,MMM,JNJ,CAT,MRK,MSFT: symbols to query, maximum 1000 ticker symbols
	- startDate: Query from this date