package com.tuan_le.stock_price.config;

import org.ehcache.event.CacheEvent;
import org.ehcache.event.CacheEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CacheEventLogger<K, V> implements CacheEventListener<K, V> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CacheEventLogger.class);

    @Override
    public void onEvent(CacheEvent<? extends K, ? extends V> cacheEvent) {
        LOGGER.debug(
                "Event: " + cacheEvent.getType() + " Key: " + cacheEvent.getKey() + " old value: "
                        + cacheEvent.getOldValue() + " new value: " + cacheEvent.getNewValue());
    }
}
