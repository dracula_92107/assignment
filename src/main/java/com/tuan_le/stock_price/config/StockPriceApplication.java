package com.tuan_le.stock_price.config;

import java.util.concurrent.Executor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import com.tuan_le.stock_price.service.QueryService;
import com.tuan_le.stock_price.service.StockPriceService;

@SpringBootApplication
@ComponentScan("com.tuan_le")
@EnableAsync
@EnableCaching
public class StockPriceApplication {

    public static void main(String[] args) {
        SpringApplication.run(StockPriceApplication.class, args);
    }

    @Bean
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(1000);
        executor.setMaxPoolSize(1000);
        executor.setQueueCapacity(1000);
        executor.setThreadNamePrefix("StockPriceService-");
        executor.initialize();
        return executor;
    }

    @Bean
    public StockPriceService stockPriceService() {
        return new StockPriceService();
    }

    @Bean
    public QueryService queryService() {
        return new QueryService();
    }
}
