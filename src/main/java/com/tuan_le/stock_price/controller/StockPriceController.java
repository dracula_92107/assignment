package com.tuan_le.stock_price.controller;

import java.util.AbstractMap.SimpleEntry;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.tuan_le.stock_price.service.StockPriceService;

@RestController
@RequestMapping("/api/v2")
public class StockPriceController {

    @Autowired
    private StockPriceService stockPriceService;

    @GetMapping("/{symbol}/closePrice")
    public SimpleEntry<String, List<SimpleEntry<String, Object>>> getClosePrice(
            @PathVariable("symbol") String ticketSymbol,
            @RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate) {

        return stockPriceService.getClosePrice(ticketSymbol, startDate, endDate);
    }

    @GetMapping("/{symbol}/200dma")
    public SimpleEntry<String, Map<String, String>> get200dma(@PathVariable String symbol,
            @RequestParam("startDate") String startDate) {

        return stockPriceService.get200dma(symbol, startDate);
    }

    @GetMapping("/{symbols}/multi200dma")
    public SimpleEntry<String, List<Map<String, String>>> getMultiple200dma(
            @PathVariable List<String> symbols, @RequestParam("startDate") String startDate) {

        return stockPriceService.get200dmaForMultiTicket(symbols, startDate);
    }

}
