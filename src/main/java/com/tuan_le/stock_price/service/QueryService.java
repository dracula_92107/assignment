package com.tuan_le.stock_price.service;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.client.RestTemplate;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class QueryService {

    private static final String END_OF_DAY_STOCK_PRICE_URL =
            "https://www.quandl.com/api/v3/datasets/EOD/%s/data.json?start_date=%s&end_date=%s&api_key=tu3ahDuGzefPLdEXKksY";

    private Gson gson = new Gson();

    @Async
    public CompletableFuture<JsonArray> asyncQueryData(String ticketSymbol,
            String startDateInString, String endDateInString) {

        String dataString = queryData(ticketSymbol, startDateInString, endDateInString);
        return CompletableFuture.completedFuture(gson.fromJson(dataString, JsonArray.class));
    }

    @Cacheable(value = "stockPriceCache",
            key = "#ticketSymbol.concat('-').concat(#startDateInString).concat('-').concat(#endDateInString)")
    public String queryData(String ticketSymbol, String startDateInString, String endDateInString) {
        String url = String.format(END_OF_DAY_STOCK_PRICE_URL, ticketSymbol, startDateInString,
                endDateInString);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new StockPriceResponseErrorHandler());
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        return Optional.ofNullable(response)
                .map(res -> gson.fromJson(res.getBody(), JsonObject.class))
                .map(jsonObj -> jsonObj.get("dataset_data")).map(JsonElement::getAsJsonObject)
                .map(datasetData -> datasetData.get("data")).map(JsonElement::getAsJsonArray)
                .map(Object::toString).orElse(null);
    }
}
