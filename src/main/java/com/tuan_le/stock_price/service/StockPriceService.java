package com.tuan_le.stock_price.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import com.google.gson.Gson;
import com.google.gson.JsonArray;

public class StockPriceService {

    private static final String TICKER = "Ticker";
    private static final int DATE_DATA_INDEX = 0;
    private static final int CLOSE_DATE_INDEX = 4;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private Gson gson = new Gson();

    @Autowired
    private QueryService queryService;

    public SimpleEntry<String, List<SimpleEntry<String, Object>>> getClosePrice(String ticketSymbol,
            String startDateInString, String endDateInString) {

        validateInputClosePrice(ticketSymbol, startDateInString, endDateInString);

        JsonArray data = gson.fromJson(
                queryService.queryData(ticketSymbol, startDateInString, endDateInString),
                JsonArray.class);

        List<SimpleEntry<String, Object>> prices = extractClosePrice(ticketSymbol, data);

        return new SimpleEntry<>("Prices", prices);
    }

    private void validateInputClosePrice(String ticketSymbol, String startDateInString,
            String endDateInString) {
        if (StringUtils.isBlank(ticketSymbol)) {
            throw new IllegalArgumentException("Ticket symbol cannot be blank.");
        }

        if (StringUtils.isAnyBlank(startDateInString, endDateInString)) {
            throw new IllegalArgumentException("startDate, endDate cannot be blank");
        }

        Date start = parseDate(startDateInString);
        Date end = parseDate(endDateInString);

        if (null == start || null == end || start.after(end)) {
            throw new IllegalArgumentException("Invalid startDate and endDate");
        }
    }

    private Date parseDate(String dateInString) {
        Date date = null;
        try {
            date = simpleDateFormat.parse(dateInString);
        } catch (ParseException e) {
            // TODO: log
            System.err.println(e);
        }
        return date;
    }

    private List<SimpleEntry<String, Object>> extractClosePrice(String ticketSymbol,
            JsonArray data) {
        if (data == null || data.size() == 0) {
            throw new IllegalArgumentException("Data not found.");
        }
        List<SimpleEntry<String, Object>> prices = new ArrayList<>();
        prices.add(new SimpleEntry<String, Object>(TICKER, ticketSymbol));
        List<String[]> dataClose = new LinkedList<>();
        data.forEach(d -> {
            JsonArray dayData = d.getAsJsonArray();
            dataClose.add(new String[] {dayData.get(DATE_DATA_INDEX).getAsString(),
                    dayData.get(CLOSE_DATE_INDEX).getAsString()});
        });

        prices.add(new SimpleEntry<String, Object>("DateClose", dataClose));
        return prices;
    }

    public SimpleEntry<String, Map<String, String>> get200dma(String ticketSymbol,
            String startDateInString) {
        if (StringUtils.isBlank(ticketSymbol)) {
            throw new IllegalArgumentException("Ticket symbol cannot be blank.");
        }

        if (StringUtils.isBlank(startDateInString)) {
            throw new IllegalArgumentException("startDate cannot be blank");
        }

        String endDateInString = calculate200dmaEndTime(startDateInString);

        JsonArray data = gson.fromJson(
                queryService.queryData(ticketSymbol, startDateInString, endDateInString),
                JsonArray.class);
        if (data == null || data.size() == 0) {
            throw new IllegalArgumentException(anotherDateSuggestion());
        }
        double average = calculateAverage(data);

        Map<String, String> dma = buildDmaBlock(ticketSymbol, average);

        return new SimpleEntry<>("200dma", dma);
    }

    private String anotherDateSuggestion() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -200);
        return String.format(
                "Data not found. Please make sure the ticket and the date are correct. Try again with date %s.",
                simpleDateFormat.format(cal.getTime()));
    }

    private Map<String, String> buildDmaBlock(String ticketSymbol, double average) {
        Map<String, String> dma = new HashMap<>();
        dma.put(TICKER, ticketSymbol);
        dma.put("Avg", String.format("%.2f", average));
        return dma;
    }

    private double calculateAverage(JsonArray data) {
        int size = data.size();

        double total = 0;

        for (int i = 0; i < size; i++) {
            JsonArray dayData = data.get(i).getAsJsonArray();
            total += dayData.get(CLOSE_DATE_INDEX).getAsDouble();
        }
        return total / size;
    }

    private String calculate200dmaEndTime(String startDateInString) {
        Date start = parseDate(startDateInString);
        if (null == start) {
            throw new IllegalArgumentException("Invalid startDate");
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(start);
        cal.add(Calendar.DATE, 200);
        return simpleDateFormat.format(cal.getTime());
    }

    public SimpleEntry<String, List<Map<String, String>>> get200dmaForMultiTicket(
            List<String> ticketSymbols, String startDateInString) {

        if (StringUtils.isBlank(startDateInString)) {
            throw new IllegalArgumentException("startDate cannot be blank");
        }

        if (CollectionUtils.isEmpty(ticketSymbols)) {
            throw new IllegalArgumentException("Invalid ticket symbols");
        }

        if (ticketSymbols.size() > 1000) {
            throw new IllegalArgumentException("Maximum number of ticket symbols is 1000");
        }

        final String endDateInString = calculate200dmaEndTime(startDateInString);

        List<SimpleEntry<String, CompletableFuture<JsonArray>>> requests = ticketSymbols.stream()
                .map(symbol -> new SimpleEntry<>(symbol,
                        queryService.asyncQueryData(symbol, startDateInString, endDateInString)))
                .filter(Objects::nonNull).collect(Collectors.toList());

        CompletableFuture.allOf((requests.stream().map(SimpleEntry::getValue)
                .collect(Collectors.toList()).toArray(new CompletableFuture[0]))).join();

        List<Map<String, String>> multipleDma = requests.stream().map(req -> {
            try {
                JsonArray data = req.getValue().get();
                if (data == null || data.size() == 0) {
                    Map<String, String> dmaError = new HashMap<>();
                    dmaError.put(TICKER, req.getKey());
                    dmaError.put("Error", anotherDateSuggestion());
                    return dmaError;
                }
                double average = calculateAverage(data);
                return buildDmaBlock(req.getKey(), average);
            } catch (InterruptedException | ExecutionException e) {
                // TODO: log
                return null;
            }
        }).collect(Collectors.toList());

        return new SimpleEntry<>("200dma", multipleDma);
    }

}
