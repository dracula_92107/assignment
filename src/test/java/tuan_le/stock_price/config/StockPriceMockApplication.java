package tuan_le.stock_price.config;

import org.mockito.Mockito;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import com.tuan_le.stock_price.service.QueryService;

@Profile("test")
@SpringBootApplication
public class StockPriceMockApplication {

    public static void main(String[] args) {
        SpringApplication.run(StockPriceMockApplication.class, args);
    }

    @Bean
    @Primary
    public QueryService nameService() {
        return Mockito.mock(QueryService.class);
    }
}
